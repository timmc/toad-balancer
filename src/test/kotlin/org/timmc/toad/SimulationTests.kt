package org.timmc.toad

import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.atomic.AtomicLong
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

class SimulationTests {
    @Test
    fun testHealthyIsEven() {
        val lb = TestUtils.makeSteadyLB3()

        val numCalls = 10_000
        val averageCallIntervalMillis = 50
        val callLatencyMs = 100L

        val start = lb.clock.instant()
        val results = runLBSimulation(lb, start, sequence {
            var callTime = start
            repeat(numCalls) {
                callTime = callTime.plusMillis(
                    Random.nextLong(averageCallIntervalMillis * 2L)
                )
                yield(LBCall(callTime, callLatencyMs, null))
            }
        })

        // Just a test of the simulator itself, not specific to the balancer.
        assertContains(
            (numCalls * averageCallIntervalMillis).offBy(1.1).toLongRange(),
            results.duration.toMillis()
        )

        // The disparity of calls made should be pretty low
        assertContains(
            1.0.offBy(1.1),
            maxDisparity(results.callsByNode.map { it.value.toFloat() })
        )

        val weightDisparities = results.metricsSnapshots.map { it.weightDisparity }

        // Expect that weight disparity is rarely over a
        val lowDisparityFrac = TestUtils.fractionBelow(weightDisparities, 3f)
        assertContains(0.95..1.0, lowDisparityFrac)

        // TODO actually test error rates, load characteristics, etc.
    }
}

/**
 * The behavior of a simulated server during one load-balancer call.
 */
data class LBCall(
    /** When the call will take place, in simulation time. */
    val time: Instant,
    /** How long the call will take before returning or throwing. */
    val latencyMs: Long,
    /** An exception to throw, optionally. */
    val error: Throwable?,
)

/**
 * Information about a load balancer at a moment in time.
 */
data class MetricsSnapshot(
    val time: Instant,
    val concurrencyMin: Long,
    val concurrencyMax: Long,
    val weightMin: Float,
    val weightMax: Float,
) {
    val weightDisparity = weightMax / weightMin
}

/** The results of a simulation run. */
data class SimResults<A>(
    /**
     * How long it took to run (in model time).
     */
    val duration: Duration,
    /**
     * Snapshot metrics collected at intervals throughout the run.
     */
    val metricsSnapshots: List<MetricsSnapshot>,
    /** How many calls each node received. */
    val callsByNode: Map<A, Int>,
)

/**
 * Simulate calls to a load balancer over time and report the results.
 *
 * The load balancer's clock will be modified by the simulation, along with its
 * node metrics.
 *
 * @param lb A configured load balancer
 * @param start The time at which the simulation starts (most likely
 *   `lb.clock.instant()`)
 * @param calls All of the calls that will be made to the load balancer
 */
fun <A> runLBSimulation(
    lb: Balancer<A>, start: Instant, calls: Sequence<LBCall>
): SimResults<A> {
    val sim = TimelineSimulator()

    val callsByNode = lb.nodes.associate { it.address to AtomicLong(0) }

    fun makeCall(call: LBCall) = Runnable {
        sim.startThread {
            // Advance the load balancer's clock to the moment of the call
            lb.clock = Clock.fixed(call.time, TestUtils.utc)
            // TODO There's a race condition in the clock here, any way to fix
            //   it? Probably only matters during the warmup period, though.
            lb.call { address: A ->
                callsByNode[address]!!.incrementAndGet()
                sim.blockUntil(call.time.plusMillis(call.latencyMs))
                call.error?.let { throw it }
            }
        }
    }

    // Fill the queue in advance with all of the calls that will ever happen
    var callsToMake = 0
    calls.forEach { call ->
        sim.schedule(call.time, makeCall(call))
        callsToMake++
    }

    // Also add metrics sampling calls
    val end = calls.maxBy { it.time }.let { it.time.plusMillis(it.latencyMs) }
    val sampleIntervalMs = 10
    val numSamples = ChronoUnit.MILLIS.between(start, end) / sampleIntervalMs + 1
    val snapshots = mutableListOf<MetricsSnapshot>()
    (0 until numSamples).forEach { i ->
        val ts = start.plusMillis(i * sampleIntervalMs)
        sim.schedule(ts) {
            lb.clock = Clock.fixed(ts, TestUtils.utc)
            snapshots.add(snapshotLBMetrics(lb))
        }
    }

    val results = sim.runToCompletion(start)
    assertEquals(callsToMake + numSamples, results.eventsRun.toLong())

    return SimResults(
        duration = results.modelTimeElapsed,
        metricsSnapshots = snapshots.toList(),
        callsByNode = callsByNode.mapValues { it.value.get().toInt() }
    )
}

fun <A> snapshotLBMetrics(lb: Balancer<A>): MetricsSnapshot {
    val conc = lb.nodes.map { it.concurrency.get() }
    val weights = lb.nodes.map { lb.weight(it) }
    return MetricsSnapshot(
        time = lb.clock.instant(),
        concurrencyMin = conc.min(),
        concurrencyMax = conc.max(),
        weightMin = weights.min(),
        weightMax = weights.max(),
    )
}
