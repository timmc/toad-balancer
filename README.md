# Toad Balancer: A stochastic, adaptive load balancer

Toad Balancer is a load balancer designed to gracefully handle a wide
range of bad server behaviors by using the following principles:

- Passive measurements of actual calls give superior information
  compared to active health-check probes.
- Any server's health can only be understood relative to the entire
  cluster's health.
- Continuous (rather than discrete) decision-making reduces unwanted
  oscillations.

It can be used for any kind of network call, including HTTP proxying,
but the original design was for a client-side load balancer for
reading from a Redis cache cluster.

## How it works

As calls are made, their time-to-complete and success/failure rate are
tracked using exponentially decaying measures, and combined with
call-concurrency and node age to derive a *weight* for each
node. Nodes are selected for use based on weighted random selection.
Rather than being "kicked out of the load balancer", failing nodes are
given progressively less and less traffic.

Additional care is given to edge cases such as system startup, node
failure and recovery, and node addition and removal.

## Usage

Toad Balancer is written in Kotlin, although it should be usable from
any JVM language.

The load balancer maintains a list of "addresses"; in this example
they're strings but in practice they can be anything with a working
`equals()` function that gives enough information to make a
call. Likely you'd want to use (or wrap) HttpConnectionPool or
RedisClient objects or similar. Here, we just have hostname/port pairs
for the sake of an example:

```
val lb = Balancer<String>()
lb.setAddresses(setOf("replica-a:6789", "replica-b:6789", "replica-c:6789"))
```

This makes a load balancer that knows about three nodes with the given
addresses. This must be maintained as a long-lived object since it
will track performance information over time.

To make a call, you just pass a function to the `call()` method, which
picks a node and passes its address object to your function:

```
val result = lb.call { makeCallUsingAddress(it) }
```

There are no health checks to configure. However, it is recommended
that you set the tuning parameters on the load balancer when
constructing it; these are how you communicate to the load balancer
how much you care about latency vs. failure rates, how soon a failure
should be detected, and other preferences specific to your setup.

## More information

For a full discussion of the design principles, read
<https://www.brainonfire.net/blog/2019/07/21/load-balancing-beyond-healthchecks/>.

## License

© 2023 Tim McCormack; open source license TBD but will likely be very
permissive. If you want to use this in your application or service,
let me know, and I can go ahead and pick a license.
