package org.timmc.toad

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.fail

/**
 * Synthetic tests of how different latency, concurrency, failure rate, and
 * post-addition timing affect the *weights* of the nodes.
 */
class WeightTests {
    /**
     * At service start-time, all nodes have been newly added.
     */
    @Test
    fun testColdStart() {
        val lb = TestUtils.makeSteadyLB3()
        lb.clock = TestUtils.start // back to the start of the ramp-up period

        // Everything should have identical, close-to-zero weights (and we can
        // therefore infer an equal chance of being picked)
        assertEquals(List(3) { lb.minCombinedWeight }, weights(lb))
    }

    /**
     * Unused nodes after the ramp-up period should have equal weights, but much
     * larger than totally fresh nodes.
     */
    @Test
    fun testUnusedNodes() {
        val lb = TestUtils.makeSteadyLB3()
        assertMaxWeightDisparity(lb, 1.0..1.01)

        // If a node is replaced, the max disparity should become large, as
        // the new node will be at the beginning of its ramp-up period.
        lb.setAddresses(setOf("two", "three", "four"))
        assertMaxWeightDisparity(lb, 1_000.0..Double.POSITIVE_INFINITY)
    }

    /**
     * Properties of nodes that are responding to requests with a normal range
     * of latencies, all successful.
     */
    @Test
    fun testHappyNodesUnderNormalUsage() {
        repeatedlyTest {
            val lb = TestUtils.makeSteadyLB3()
            repeat(1_000) {
                lb.nodes.forEach {
                    lb.updateNode(it, true, Random.nextLong(50, 75))
                }
            }

            // Weights shouldn't be all that far apart
            assertMaxWeightDisparity(lb, 1.0..1.3)
        }
    }

    /**
     * A node returning errors should be discovered in a reasonable number of
     * calls.
     */
    @Test
    fun testCallsToDiscoverErrors() {
        // Latency to use in weight updates
        val avgLatency = 50L

        repeatedlyTest {
            val lb = TestUtils.makeSteadyLB3()
            // Initialize as happy
            repeat(100) {
                lb.nodes.forEach { lb.updateNode(it, true, avgLatency) }
            }

            // Start failing the calls
            val calls = countCallsUntilHighDisparity(lb) { bad ->
                lb.updateNode(bad, false, avgLatency)
            }

            // We expect a totally-failing node to be deprioritized within 10-30
            // calls.
            assertContains(20.offBy(1.5), calls)
        }
    }

    /**
     * A node with high latency should be discovered in a reasonable number of
     * calls.
     */
    @Test
    fun testCallsToDiscoverLatency() {
        // Latency to use in weight updates
        val avgHealthyLatency = 50L
        val avgUnhealthyLatency = 60 * avgHealthyLatency

        repeatedlyTest {
            val lb = TestUtils.makeSteadyLB3()
            // Initialize as happy
            repeat(100) {
                lb.nodes.forEach { lb.updateNode(it, true, avgHealthyLatency) }
            }

            // Now make node 0 high-latency
            val calls = countCallsUntilHighDisparity(lb) { bad ->
                lb.updateNode(bad, false, avgUnhealthyLatency)
            }

            // We expect a high-latency node to be deprioritized within 10-30
            // calls.
            //
            // Note that a node with high latency will also have a higher
            // concurrency as a baseline, so the node will actually be
            // deprioritized *faster* than the threshold considered here.
            assertContains(20.offBy(1.5), calls)
        }
    }

    /**
     * When one node has 5x the latency of the others, its weight should be
     * lower, but not *too* dramatically lower.
     */
    @Test
    fun testOneHigherLatency() {
        repeatedlyTest {
            val lb = TestUtils.makeSteadyLB3()
            repeat(1_000) {
                lb.nodes.forEach {
                    val latency = when (it.address) {
                        "one" -> Random.nextLong(250, 350)
                        else -> Random.nextLong(50, 75)
                    }
                    lb.updateNode(it, true, latency)
                }
            }

            // Weights shouldn't be all that far apart
            assertMaxWeightDisparity(lb, 3.0..6.0)
        }
    }

    /**
     * When one node is being used more than the others, it should get weighted
     * lower.
     */
    @Test
    fun testConcurrencyFactor() {
        val lb = TestUtils.makeSteadyLB3()
        lb.nodes[0].concurrency.set(2)
        lb.nodes[1].concurrency.set(2)
        lb.nodes[2].concurrency.set(8)

        assertMaxWeightDisparity(lb, 3.0..5.0)
    }
}

/**
 * Repeatedly update node 0 until a high disparity (1000:1) is reached among
 * the nodes of the load balancer.
 *
 * Returns the number of calls required to reach this disparity.
 */
fun <A> countCallsUntilHighDisparity(
    lb: Balancer<A>, firstNodeUpdater: (Node<A>) -> Unit
): Int {
    // How much disparity between weights would we think of as "yes,
    // some node is heavily deprioritized"?
    val detectionThreshold = 1000

    val bad = lb.nodes[0]
    var calls = 0
    while (calls < 10000000) { // something large but not infinite
        if (maxDisparity(lb) > detectionThreshold) {
            return calls
        }
        firstNodeUpdater(bad)
        calls++
    }
    fail("Did not reach high disparity even after a large number of calls")
}
