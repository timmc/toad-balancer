package org.timmc.toad

/**
 * Given a collection of items and their respective weights, pick one at random,
 * with likelihood proportional to its weight.
 *
 * If all weights are zero, a random item will be chosen.
 */
fun <I> weightedRandomSelect(options: List<Pair<I, Float>>): I {
    if (options.isEmpty())
        throw IllegalArgumentException("Cannot choose from empty list")

    val total = options.map { it.second }.sum()
    var dartDistance = Math.random() * total

    // Imagine the items as a sequence of blocks of varying widths, all strung
    // together, like so:
    //
    //     |______|_|____|____________________|
    //
    // "Throw a dart" at that set of blocks, then walk through the blocks to see
    // when we reach the block that contains the dart.
    //
    //     |______|_|__X_|____________________|
    //              ^
    // Here, the caret marks the start of the item we're currently considering,
    // and the X marks the dart.
    //
    //              |__X_|____________________|
    //
    // We'll calculate this by setting `dartDistance` initially to something
    // from 0 to total, and check if it's less than the first block's width.
    // If so, return the block's item; if not, subtract that width from the
    // distance and move on to the next block.

    val candidates = options.shuffled() // in case of zero-widths and such
    for ((item, width) in candidates) {
        if (dartDistance < width) {
            // The dart has landed inside this item's width.
            return item
        } else {
            dartDistance -= width
        }
    }

    // Fallback case. Probably a bunch of zero widths, or there's a floating
    // point error or something. No matter, it's shuffled, so just pick the last
    // one (most appropriate in case of floating point error.)
    return candidates.last().first
}
