package org.timmc.toad

import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class WeightedRandomSelectionTests {
    /**
     * Throws an expected exception when given an empty list.
     */
    @Test
    fun testEmptyList() {
        assertFailsWith(IllegalArgumentException::class) {
            weightedRandomSelect(emptyList<Pair<String, Float>>())
        }
    }

    /**
     * Properly handles a single-element list.
     */
    @Test
    fun testSingle() {
        val single = listOf("a" to 0.5f)
        val picks = generateSequence { weightedRandomSelect(single) }
            .take(3)
            .toList()

        assertEquals(listOf("a", "a", "a"), picks)
    }

    /**
     * When all weights are zero, pick fully randomly.
     */
    @Test
    fun testAllZeroes() {
        val allZero = listOf("a" to 0f, "b" to 0f, "c" to 0f)
        val picks = generateSequence { weightedRandomSelect(allZero) }
            .take(300_000)
            .frequencies()

        allZero.forEach { (k, _) ->
            assertContains(90_000..110_000, picks[k])
        }
    }

    /**
     * Check that elements are picked in proportion to their weights.
     */
    @Test
    fun testRatios() {
        val weights = listOf("a" to 10f, "b" to 30f, "c" to 60f)
        val picks = generateSequence { weightedRandomSelect(weights) }
            .take(300_000)
            .frequencies()
            .toMap()

        assertContains(2.9..3.1, picks["b"]!!.toDouble()/picks["a"]!!)
        assertContains(5.9..6.1, picks["c"]!!.toDouble()/picks["a"]!!)
        assertContains(1.9..2.1, picks["c"]!!.toDouble()/picks["b"]!!)
    }

    /**
     * When one of the weights (but not all) is zero, it is never picked, even
     * if it is in first or last position.
     */
    @Test
    fun testOneZero() {
        val firstZero = listOf("a" to 0f, "b" to 0.0001f)
        repeat(1_000_000) {
            assertEquals("b", weightedRandomSelect(firstZero))
        }

        val lastZero = listOf("a" to 0.0001f, "b" to 0f)
        repeat(1_000_000) {
            assertEquals("a", weightedRandomSelect(lastZero))
        }
    }
}
