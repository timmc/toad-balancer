package org.timmc.toad

import java.time.Clock
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.atomic.AtomicReference
import kotlin.math.pow
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

/**
 * One node in a load-balanced group.
 *
 * Nodes have an immutable address object as well as mutable state values that
 * are updated each time the node is used.
 */
class Node<A>(
    /**
     * The information needed to actually use this node.
     *
     * This might be a URL, an index into a list somewhere, or anything else
     * that the caller might want to load-balance between.
     */
    val address: A,

    /**
     * When this node was added to the rotation, in milliseconds since Unix
     * epoch.
     */
    val addedTimestampMillis: Long = System.currentTimeMillis(),
) {
    /**
     * The recency-weighted success rate of this node.
     *
     * A success is a `1`, and a failure is a `0`. Each successive "measurement"
     * is folded into the success rate by some weighted operation, so that the
     * value is more heavily influenced by the most recent activity.
     */
    val successRate = AtomicReference(1f)

    /**
     * The recency-weighted latency of calls to this node, in milliseconds.
     *
     * Each latency measurement is folded into this running value by a weighted
     * operation.
     */
    val latencyMillis = AtomicReference<Float?>(null)

    /**
     * The number of calls currently active on this node.
     */
    val concurrency = AtomicLong(0)
}

/**
 * A load balancer that operations over a set of [Node] objects.
 *
 * This load balancer passively collects latency, error rate, concurrency, and
 * age metrics for each node and combines them into a single relative weight.
 * When [call] is invoked, a node is chosen using a randomized selection
 * algorithm that preferentially (but not strictly) selects nodes with a higher
 * relative weight.
 *
 * Of note, this load balancer never fully removes an unhealthy node from
 * service; rather, the node just becomes increasingly less likely to be picked.
 *
 * This load balancer will operate best if tuned for workload characteristics
 * and operational goals. The main decisions to make are:
 *
 * - How quickly should the load balancer respond to apparent changes in the
 *   health of the nodes?
 * - What is the relative importance of each of 1) low error rate, 2) low
 *   latency, and 3) well-balanced load?
 *
 * Tuning the [successDecay] and [latencyDecay] parameters will change how
 * quickly changes in the node's success rate and response latency affect how
 * often the node is chosen for work. These are exponential decay multipliers
 * between 0 and 1. Higher numbers will cause fast responses, but will err on
 * the side of shunting work away from a node that shows signs of trouble, even
 * if that trouble is only fleeting. Lower numbers will cause a slower response
 * to poor performance, but will smooth the behavior of the load balancer and
 * prevent rapid shifting of load between nodes.
 *
 * Tuning the [successFactorWeighting], [latencyFactorWeighting], and
 * [concurrencyFactorWeighting] parameters will adjust which factors are used
 * most heavily in determining the overall health of a node, and therefore its
 * weighting during weighted-random selection. For most applications, the
 * success rate is most critical and even a low rate of errors from one node
 * should cause it to be heavily deprioritized. In contrast, it may take a large
 * disparity in latency before a node should be considered unhealthy enough to
 * deprioritize. Concurrency, similarly, is likely to be of lower concern; if
 * one is receiving only moderately more traffic than the others (perhaps due to
 * having better success rate or lower latency) then this is likely to be
 * acceptable.
 *
 * Additional parameters allow tuning of lifecycle-related issues, such as
 * [rampUpPeriodMillis] for how newly added nodes are handled and
 * [minCombinedWeight] for ensuring that recovery is detectable.
 */
class Balancer<A>(
    val successDecay: Float = 0.11f,
    val latencyDecay: Float = 0.2f,

    val successFactorWeighting: Float = 3f,
    val latencyFactorWeighting: Float = 1f,
    val concurrencyFactorWeighting: Float = 1f,

    /**
     * Provides for a ramp-up period when a new node is added to the balancer.
     *
     * All else equal, this will cause the probability of a new node being
     * selected for use to rise from 0 to 1/k (in a group of k nodes) over the
     * time span selected here.
     *
     * A short ramp-up period allows newly added capacity to be used as quickly
     * as possible, while a long ramp-up period allows the node's true health
     * to become more accurately measured before it is placed fully in service —
     * and also allows the node a chance to warm up rather than having it
     * immediately be hit by 1/k of all traffic.
     */
    val rampUpPeriodMillis: Long = 10_000,

    /**
     * This is the minimum a node's raw weight can ever be.
     *
     * If a node ever gets a raw weight that is too low, it will never be used
     * again (unless *all* of the nodes have similarly low weights). This
     * creates a data-starvation trap: A node that starts failing consistently
     * would stop getting called, but if it recovers, we'd never find out. That
     * reduces the stability of the system, since we're failing to use capacity.
     * Therefore, we need to allow even the worst-performing node a *small*
     * chance of being picked every once in a while. This ensures that if it
     * recovers, we'll eventually notice and start using it again.
     *
     * This number should be set quite low, and with reference to the expected
     * error rates, latency, and concurrency of normal traffic.
     */
    val minCombinedWeight: Float = 0.00001f,
) {
    private val latencyRetain = 1 - latencyDecay
    private val successRetain = 1 - successDecay

    internal var nodes = listOf<Node<A>>()
    private val nodeLock = Object()

    internal var clock = Clock.systemUTC()

    /**
     * Add a node with this address, if none already uses it.
     */
    fun addAddress(address: A) {
        synchronized(nodeLock) {
            if (!nodes.any { it.address == address }) {
                nodes = nodes.plus(Node(address, clock.millis()))
            }
        }
    }

    /**
     * Remove any node with this address.
     */
    fun removeAddress(address: A) {
        synchronized(nodeLock) {
            nodes = nodes.filterNot { it.address == address }
        }
    }

    /**
     * Add and remove nodes so that their addresses match this set.
     *
     * Preserves state of any nodes that already have an address in this set.
     */
    fun setAddresses(addresses: Set<A>) {
        synchronized(nodeLock) {
            val oldByAddress = nodes.associateBy(Node<A>::address)
            nodes = addresses.map { addr ->
                oldByAddress.getOrElse(addr) { Node(addr, clock.millis()) }
            }
        }
    }

    /**
     * Determine the current weight of a node.
     *
     * This is a score that is *relative* to the weights of the other nodes.
     */
    fun weight(node: Node<A>): Float {
        // Turn each metric into a factor between [0, 1], raise them each to a
        // power (in order to weight some of the individual factors more
        // heavily), and then multiply them all together to form a node weight.

        // Success rate can be used straight up, as it's already in [0, 1] in
        // the way we need.
        val successFactor = node.successRate.get().pow(successFactorWeighting)

        // Latency needs to be inverted to get "quickness", which will be inside
        // [0, x] for some x < 1.
        //
        // We add 1 to the latency so that the ratio can never exceed 1. (If the
        // latency were 0 ms, 1/(1 + 0) = 1, which is the highest we want to
        // go.) Technically, it would be OK for a factor to be greater than 1,
        // but we definitely want to avoid division by zero.
        //
        // For any operation, there is a minimum latency. Let's say the load
        // balancer is assisting with HTTP calls to another microservice in the
        // datacenter. The latency for that call might have a floor of 10 ms,
        // and perhaps we have some timeout that keeps it from going over 30 s.
        // That means that the latency factor will, in practice, be in the range
        // of [0.0000333…, 0.9090…], not [0, 1]. That's OK though, since all of
        // the nodes will be "penalized" the same amount, and the weighted
        // random selection won't be compromised.
        val latency = node.latencyMillis.get()
        val latencyFactor = if (latency == null) {
            // Never had a call yet, so give the multiplicative identity
            1f
        } else {
            // TODO Add damping factor -- maybe `minExpected/(latency + minExpected)`
            //   This would still produce an asymptotic 1/latency behavior but
            //   would ensure that instant responses are weighted no better than
            //   twice the minimum expected response.
            //
            // There's an interaction with concurrency, though -- a
            // fast-responding node will also have a lower concurrency value on
            // average.
            val quickness = 1/(latency + 1f)
            quickness.pow(latencyFactorWeighting)
        }

        // Similar to latency, nodes with more concurrent calls should have a
        // lower factor. We add 1 and invert to get something in the range of
        // [0, 1]. In practice, the values will be 1, 1/2, 1/3, etc.
        //
        // That's a large step size between a node with 0 active calls and a
        // node with 1 active call, but that's appropriate: This is a load
        // balancer! However, we do want to make sure that success rate gets a
        // much higher exponent than concurrency.
        val capacity = 1/(node.concurrency.get() + 1f)
        val concurrencyFactor = capacity.pow(concurrencyFactorWeighting)

        // This is a pseudo-metric that allows for a warm-up period. New nodes
        // have no history to draw on, so for their first N seconds of life they
        // should be less trusted. Derive a factor that ramps from 0 to 1 over
        // the first [rampUpPeriodMillis] milliseconds of the node's life.
        val age = clock.millis() - node.addedTimestampMillis
        val ageFactor = (age.toFloat() / rampUpPeriodMillis).coerceIn(0f, 1f)

        // The raw weighting is just the product of the factors.
        val raw = successFactor * latencyFactor * concurrencyFactor * ageFactor
        // But make sure it's never zero.
        return raw.coerceAtLeast(minCombinedWeight)
    }

    /**
     * Pick a node using weighted random selection based on its health.
     */
    internal fun pick(): Node<A> {
        return weightedRandomSelect(nodes.map { it to weight(it) })
    }

    /**
     * Select a node, use it, and then free it again.
     *
     * This is the main method for the load balancer. The provided function will
     * be called with the address of a node that has been selected for
     */
    @OptIn(ExperimentalTime::class)
    fun <R> call(funk: (A) -> R): R {
        if (nodes.isEmpty()) {
            throw RuntimeException("Cannot make call; no nodes in load balancer.")
        }
        val toCall = pick()

        toCall.concurrency.incrementAndGet()
        val (result, duration) = measureTimedValue {
            runCatching { funk(toCall.address) }
        }
        toCall.concurrency.decrementAndGet()
        updateNode(toCall, isSuccess=result.isSuccess, latencyMs=duration.inWholeMilliseconds)

        return result.getOrThrow()
    }

    internal fun updateNode(node: Node<*>, isSuccess: Boolean, latencyMs: Long) {
        node.successRate.updateAndGet { current ->
            val successValue = if (isSuccess) 1 else 0
            current * successRetain + successValue * successDecay
        }
        // Only incorporate new latency values on success, otherwise fast-fail
        // calls could incorrectly weight this node much more heavily.
        //
        // TODO: Also consider recording latency when there is an error of a
        //  type that is known to indicate a latency-related failure, such as
        //  SocketTimeoutException. Alternatively, only record latency values
        //  for failures when it makes the latency average worse.
        if (isSuccess) {
            node.latencyMillis.updateAndGet { current ->
                if (current == null) {
                    latencyMs.toFloat()
                } else {
                    current * latencyRetain + latencyMs * latencyDecay
                }
            }
        }
    }
}
