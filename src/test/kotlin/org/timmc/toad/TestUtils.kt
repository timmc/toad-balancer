package org.timmc.toad

import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.roundToInt
import kotlin.test.fail

object TestUtils {
    val utc: ZoneId = ZoneId.of("UTC")

    val start = Clock.fixed(Instant.parse("2020-01-01T00:00:00Z"), utc)!!

    /**
     * Make a steady-state (ramped-up) load-balancer of three nodes.
     */
    fun makeSteadyLB3(): Balancer<String> {
        return Balancer<String>().also {
            // Set the clock before adding the nodes, since it will be used when
            // setting their "added" times.
            it.clock = start
            it.setAddresses(setOf("one", "two", "three"))
            // Advance to end of ramp-up period
            it.clock = Clock.offset(start, Duration.ofMillis(it.rampUpPeriodMillis))
        }
    }

    /**
     * Compute what fraction of [numbers] are strictly less than [limit].
     */
    fun <T: Comparable<T>> fractionBelow(numbers: List<T>, limit: T): Double {
        return numbers.count { it < limit } * 1.0 / numbers.size
    }
}

/**
 * Count the number of occurrences of elements in a sequence.
 */
fun <T> Sequence<T>.frequencies(): Map<T, Int> {
    return groupBy { it }.mapValues { it.value.size }
}

fun Double.offBy(mult: Double): ClosedRange<Double> {
    return (this / mult)..(this * mult)
}

/**
 * Generate an approximation range.
 *
 * For integer x, this generates x/mult..x*mult
 */
fun Int.offBy(mult: Double): ClosedRange<Int> {
    return floor(this / mult).roundToInt()..ceil(this * mult).roundToInt()
}

fun ClosedRange<Int>.toLongRange(): ClosedRange<Long> {
    return LongRange(start.toLong(), endInclusive.toLong())
}

/**
 * Run a nondeterministic test block repeatedly to increase the chances of
 * finding a false assertion.
 *
 * Runs for a minimum time and a minimum number of iterations. (No maximum --
 * that's the responsibility of the test block.)
 */
fun repeatedlyTest(minSeconds: Int = 1, minIters: Int = 100, tests: () -> Unit) {
    val startNanos = System.nanoTime()
    val endNanos = startNanos + minSeconds * 1_000_000_000L
    while (true) {
        repeat(minIters) {
            tests()
        }
        if (System.nanoTime() > endNanos) {
            break
        }
    }
}

/** Compute the weights of the nodes in the load balancer. */
fun <A> weights(lb: Balancer<A>): List<Float> {
    return lb.nodes.map(lb::weight)
}

/** Largest ratio between weights of any two nodes in the load balancer. */
fun maxDisparity(lb: Balancer<*>): Double {
    return maxDisparity(weights(lb))
}

/** Largest ratio between any two weights in the list. */
fun maxDisparity(weights: List<Float>): Double {
    return (weights.max() / weights.min()).toDouble()
}

/**
 * Assert that the max disparity of the node weights is in the given range.
 */
fun <A> assertMaxWeightDisparity(
    lb: Balancer<A>, range: ClosedFloatingPointRange<Double>
) {
    assertMaxDisparityIn(weights(lb), range.start, range.endInclusive)
}

/**
 * Assert that the max disparity among weights is >= low and <= high.
 */
fun assertMaxDisparityIn(weights: List<Float>, low: Double, high: Double) {
    val ratio = maxDisparity(weights)
    if (ratio < low) {
        fail("Max disparity $ratio unexpectedly lower than $low for weights $weights")
    } else if (ratio > high) {
        fail("Max disparity $ratio unexpectedly higher than $high for weights $weights")
    }
}
