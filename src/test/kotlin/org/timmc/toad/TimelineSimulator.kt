package org.timmc.toad

import java.time.Duration
import java.time.Instant
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.PriorityBlockingQueue
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

/**
 * Simulates events occurring at various points in time, while not actually
 * requiring the time between events to elapse.
 *
 * Events take the form of [Runnable]s that are scheduled for future [Instant]s
 * on a timeline. They are called when that point on the timeline is reached.
 * Event functions are also able to [schedule] further events on the timeline.
 * (Nominally, these should be in the future of the first event, but events
 * scheduled for the past will be run as well, not lost.)
 */
class TimelineSimulator {
    /**
     * The timeline queue contains all events that have not yet executed, in
     * timeline order. The simulation does not exit until this is back to zero.
     *
     * There is no fixed concept of "now", as it can be any point before the
     * first event in the queue.
     */
    private val timeline = PriorityBlockingQueue<FutureEvent>()

    /**
     * The thread pool is used for managing any threads the user needs to
     * run via [startThread].
     */
    private val threadPool = Executors.newCachedThreadPool()

    /**
     * How many threads are still at-large, and may not have finished their work
     * yet (and may still intend to add a future event to the timeline). The
     * simulation does not exit until this is back to zero.
     */
    private val runningThreads = AtomicLong(0)

    /**
     * An event on the timeline. If it is on the timeline, it is by definition
     * in the future. FutureEvent is comparable by activation timestamp.
     */
    private data class FutureEvent(
        /**
         * Marks an event as having been submitted by user code (as opposed to
         * internal events generated by [blockUntil]).
         */
        val external: Boolean,
        /** The simulation-time at which to run the task. */
        val activateAt: Instant,
        /** Code to run. */
        val task: Runnable,
    ): Comparable<FutureEvent> {
        override fun compareTo(other: FutureEvent) =
            activateAt.compareTo(other.activateAt)
    }

    /**
     * Schedule a task to run at some future time in the simulation.
     *
     * The task must make some accommodations in order to work within the
     * simulation:
     *
     * - Rather than starting a Thread on its own, call [startThread]
     * - Rather than calling Thread.sleep, call [blockUntil]
     */
    fun schedule(runAt: Instant, task: Runnable) {
        timeline.put(FutureEvent(true, runAt, task))
    }

    /**
     * Start a thread within the simulation.
     *
     * The advantage of this over creating a Thread directly is that
     * [startThread] allows tracking unfinished threads. This prevents the
     * simulation from ending while there are still threads at work.
     */
    fun startThread(task: () -> Unit) {
        runningThreads.incrementAndGet()
        threadPool.submit {
            try {
                task()
            } finally {
                runningThreads.decrementAndGet()
            }
        }
    }

    /**
     * Block the current thread until the given time in the simulation.
     *
     * This must be used rather than Thread.sleep in order to stay aligned with
     * the simulated timeline.
     */
    fun blockUntil(until: Instant) {
        val lock = CountDownLatch(1)
        timeline.put(FutureEvent(false, until, lock::countDown))
        lock.await()
    }

    /**
     * Run the simulation until all tasks and threads are completed.
     *
     * This method may error if called more than once.
     *
     * @param simulationStart Nominal start-time of the simulation, used to
     *   calculate the simulation's total run time.
     */
    fun runToCompletion(simulationStart: Instant): RunResults {
        var lastTime: Instant = simulationStart
        var externalEventsRun = 0

        // The delicate bit here is that as we reach the end of the queue, the
        // last events may launch threads that then are scheduled to block for
        // later; those events may be scheduled *after* the queue has run empty.
        // This means we may need to repeatedly check for both empty queue and
        // zero thread count.
        while (true) {
            // Run through the queue until we run out of work
            while (timeline.isNotEmpty()) {
                val event = timeline.take()
                event.task.run()
                lastTime = event.activateAt
                if (event.external) {
                    externalEventsRun++
                }
            }

            // Check if there are any open threads. If there aren't, it's still
            // possible that one has *just now* finished, and dropped something
            // into the queue! So we have to re-check the queue.
            //
            // If there are no threads and still no new tasks, it's OK to exit.
            if (runningThreads.get() == 0L && timeline.isEmpty()) {
                threadPool.shutdown()
                threadPool.awaitTermination(10, TimeUnit.SECONDS)

                // One final check, now that the thread runner has shut down;
                // having checked runningThreads should be sufficient, but this
                // extra check shouldn't hurt.
                if (timeline.isEmpty()) {
                    break
                }
            }

            // Sleep a little bit to allow any active threads to do their work
            // (i.e. drop more events into the queue.)
            Thread.sleep(50)
        }

        return RunResults(
            modelTimeElapsed = Duration.between(simulationStart, lastTime),
            eventsRun = externalEventsRun,
        )
    }

    data class RunResults(
        /**
         * The duration, in simulation-time, from start to end. The end is the
         * time the last task was scheduled for, or the time at which the last
         * thread was scheduled to block until, whichever comes last.
         */
        val modelTimeElapsed: Duration,

        /**
         * How many scheduled events were processed.
         */
        val eventsRun: Int,
    )
}
